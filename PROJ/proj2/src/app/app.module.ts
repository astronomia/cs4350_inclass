import { BrowserModule } from 
'@angular/platform-browser';
import { NgModule } from 
'@angular/core';

// httpModule!
import { HttpClientModule } from '@angular/common/http'

//angular forms
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from 
'./app-routing.module';
import { AppComponent } from 
'./app.component';
// new components will be imported whenever ng generate components ...
import { TodoComponent } from 
'./components/todo/todo.component';
import { TodoItemComponent } from 
'./components/todo-item/todo-item.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { AddTodoComponent } from './components/add-todo/add-todo.component';

@NgModule({        // the root app module of Angular
  declarations: [
    AppComponent,
    TodoComponent,
    TodoItemComponent,
    HeaderComponent,
    AddTodoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //HttpM. Added here, becasue it is imported ?
    HttpClientModule,
    //Forms Module
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
