// add Input from /core
// add EventEmitter, Output
import { Component, OnInit, Input, EventEmitter, Output} 
from '@angular/core';

import { TodoService } 
from '../../services/todo.service'

import { Todo } 
from 'src/app/models/Todo';


@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  // take something in
  @Input() todo: Todo;  // input component
  // emit something out
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();
  constructor(private todoService:TodoService) { }

  ngOnInit() {
  }

  //Set Dynamic Classes
  setClasses(){
    /* let.. classes =... */
    let classes ={
      todo: true,
      //change deco as defined in todo-item...css
      'is-completed': this.todo.completed  // dynamic class binding 
    }
    return classes;
  }


    //On Click Events:
    //e.g. calling methods of console logs
    // or be like line through todos

    // but this is only on UIs

    onToggle(todo) {
      // console.log('toggle');

      //Toggle in Ui, and console log
      this.todo.completed = !this.todo.completed;
      console.log(this.todo.completed);
      //Toggle on server
      this.todoService.toggleCompleted(todo).subscribe(todo => console.log(todo));

    }
    onDelete(todo) {
      //Toggle by console log
      //  console.log('delete');
      this.deleteTodo.emit(todo);
    
    }

}
