import { Component, OnInit } from '@angular/core';
//use service.ts from services
import {TodoService} from '../../services/todo.service'

import { Todo } from '../../models/Todo'// import Todo.ts from models
import { combineLatest } from 'rxjs';
 
@Component({
  selector: 'app-todos',   // specify the component: app-todo
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  todos:Todo[];   // typed as Todo[]

  //  constuctor: only to import services
  constructor(private todoService:TodoService) {}   

  //ngOnInit is used mainly



  ngOnInit() {   
    //subscribe to Observable, an async data stream
     this.todoService.getTodos().subscribe(todos => {
       this.todos = todos;
     }); 
  }


  deleteTodo(todo:Todo){
    //console.log("deleted!")

    //Removal from UI
    this.todos = this.todos.filter( t => t.id !== todo.id ); // make id disappear
    //Removal from server
    this.todoService.deleteTodo(todo).subscribe();
  }



  // Add Todo
  addTodo(todo:Todo) {
    this.todoService.addTodo(todo).subscribe( todo => { 
      this.todos.push(todo); 
    });
  }

}
