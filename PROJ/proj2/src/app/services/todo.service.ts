// allow us to inject it into a constructor/ component
// kinda backend, used to manipulate data
import { Injectable } from '@angular/core';
// rxjs is short for 'react extension'
import { Observable } from 'rxjs';
// import HttpClient, as well as Headers
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Todo } from '../models/Todo'

// def httpOptions for put
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}


@Injectable({
  providedIn: 'root'
})
export class TodoService {

  todosUrl:string =  'https://jsonplaceholder.typicode.com/todos?_limit=5';
  todosLimit = '?_limit=10'

  constructor(private http:HttpClient) { }  // web server
  
  //Get Todos
  getTodos():Observable<Todo[]> {
    return this.http.get<Todo[]>(`${this.todosUrl}${this.todosLimit}`);
  }

  //Delete todo
  deleteTodo(todo:Todo):Observable<Todo> {
    // http delete request
    const url = `${this.todosUrl}/${todo.id}`;
    return this.http.delete<Todo>(url, httpOptions);
  }

  //Add todo
  addTodo(todo:Todo):Observable<Todo> {
    return this.http.post<Todo>(this.todosUrl, todo, httpOptions);  // must return <Todo> type here
  }

  //Toggle Completed
  toggleCompleted(todo:Todo):Observable<any> {
    const url =`${this.todosUrl}/${todo.id}`;  
    //http put request
    return this.http.put<Todo>(url, todo, httpOptions);
  }

}
