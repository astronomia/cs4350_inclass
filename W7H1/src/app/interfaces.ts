export interface ISocketMessage {
    numUsers?: number; //? means optional
    username?: string;
    message?: string;
    isLog?: boolean;
    color?: string;
    fade?:boolean;
}