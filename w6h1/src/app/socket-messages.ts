export const SocketMessage = {
    // making an object separate containing all components for messages. //
    Welcome: 'Welcome to CS 4350 chat!',
    Disconnected: 'You have been disconnected.',
    Reconnected: 'You have been reconnected.',
    UnableToReconnect: 'Attempt to reconnect has failed.'
};

